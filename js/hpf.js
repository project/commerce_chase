(function($, Drupal, window) {

  Drupal.behaviors.chaseOrbitalHpf = {
    attach: function (context, settings) {
      window.cancelCREPayment = this.cancelCREPayment;
      window.whatCVV2 = this.whatCVV2;
      window.creHandleErrors = this.creHandleErrors;
      window.creHandleDetailErrors = this.creHandleDetailErrors;
      window.startCREPayment = this.startCREPayment;
      window.completeCREPayment = this.completeCREPayment;

      // Hide the next checkout step button since the iframe has its own.
      if ($('#commerce-chase-orbital-hpf-iframe').length) {
        $('#edit-actions-next').hide();
      }
      else {
        $('#edit-actions-next').show();
      }
    },
    whatCVV2: function () {
      var string = Drupal.t('The CVV Number ("Card Verification Value") on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards. On your American Express® branded credit or debit card it is a 4 digit numeric code.');
      // @todo add Drupal.theme implementation and prepend above iframe, or something.
      alert(string);
    },
    creHandleErrors: function (errorCode) {
      console.log(errorCode);
    },
    creHandleDetailErrors: function (errorCode, gatewayCode, gatewayMessage) {
      console.log(errorCode);
      $('.completeButton').prop('disabled', true);
    },
    startCREPayment: function () {
      $('.completeButton').prop('disabled', true);
    },
    completeCREPayment: function (transaction) {
      $('.chase-card-type').val(transaction.ccType);
      $('.chase-card-number').val(transaction.ccNumber);
      $('.chase-exp-month').val(transaction.expMonth);
      $('.chase-exp-year').val(transaction.expYear);
      $('.chase-remote-id').val(this.drupalSettings.commerce_chase.remoteId);
      $('form.commerce-checkout-flow').submit();
    },
    completePayment: function () {
      $('.completeButton').prop('disabled', false);
    }
  }

})(jQuery, Drupal, window);
