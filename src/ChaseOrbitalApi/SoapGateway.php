<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

use Drupal\commerce_chase\ChaseOrbitalApi\Exception\Gateway;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;

/**
 * Soap Gateway class.
 */
class SoapGateway {

  /**
   * The payment gateway entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $gateway;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $gateway
   *   The gateway entity.
   *
   * @throws \Drupal\commerce_chase\ChaseOrbitalApi\Exception\Gateway
   */
  public function __construct(PaymentGatewayInterface $gateway) {
    if ($gateway->getPluginId() !== 'chase_hpf') {
      throw new Gateway();
    }
    $this->gateway = $gateway;
  }

  /**
   * Charge a profile.
   *
   * @return \Drupal\commerce_chase\ChaseOrbitalApi\ChargeProfile
   *   The charge profile.
   */
  public function chargeProfile() {
    return new ChargeProfile($this);
  }

  /**
   * Capture a transaction.
   *
   * @return \Drupal\commerce_chase\ChaseOrbitalApi\MarkForCapture
   *   The capture soap request.
   */
  public function captureTransaction() {
    return new MarkForCapture($this);
  }

  /**
   * Void a transaction.
   *
   * @return \Drupal\commerce_chase\ChaseOrbitalApi\VoidTransaction
   *   The void transaction.
   */
  public function voidTransaction() {
    return new VoidTransaction($this);
  }

  /**
   * Fetch a profile.
   *
   * @return \Drupal\commerce_chase\ChaseOrbitalApi\ProfileFetch
   *   The profile fetch.
   */
  public function profileFetch() {
    return new ProfileFetch($this);
  }

  /**
   * Delete a profile.
   *
   * @return \Drupal\commerce_chase\ChaseOrbitalApi\ProfileDelete
   *   The profile delete.
   */
  public function profileDelete() {
    return new ProfileDelete($this);
  }

  /**
   * Get the SOAP endpoint.
   *
   * @return string
   *   The SOAP endpoint.
   */
  public function getWsdl() {
    if ($this->gateway->getPlugin()->getMode() === 'live') {
      return 'https://ws1.chasepaymentech.com/PaymentechGateway/wsdl/ChasePaymentechGateway.wsdl';
    }
    return 'https://wsvar1.chasepaymentech.com/PaymentechGateway/wsdl/ChasePaymentechGateway.wsdl';
  }

  /**
   * Gateway getter.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   *   The payment gateway entity.
   */
  public function getGateway() {
    return $this->gateway;
  }

}
