<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

/**
 * Defines a base class for requests.
 */
abstract class RequestBase implements RequestInterface {

  /**
   * The soap gateway.
   *
   * @var \Drupal\commerce_chase\ChaseOrbitalApi\SoapGateway
   */
  protected $gateway;

  /**
   * The soap client.
   *
   * @var \SoapClient
   */
  protected $soapClient;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_chase\ChaseOrbitalApi\SoapGateway $gateway
   *   The Chase gateway.
   */
  public function __construct(SoapGateway $gateway) {
    $this->gateway = $gateway;
    $options = [
      'exceptions' => TRUE,
      'trace' => TRUE,
    ];
    $this->soapClient = new \SoapClient($gateway->getWsdl(), $options);
  }

  /**
   * Send request.
   *
   * @param array $data
   *   An array of input data.
   *
   * @return object
   *   The SOAP response.
   */
  public function send(array $data) {
    $required_keys = $this->getRequiredKeys();
    $this->validateParameters($data, $required_keys);
    $request_data = $this->getParameters($data);
    $request_type = $this->getRequestType();
    return $this->soapClient->{$request_type}(new \SoapParam($request_data, $request_type));
  }

  /**
   * Validate input data.
   *
   * @param array $data
   *   The input data to validate.
   * @param array $required_keys
   *   The required keys.
   */
  protected function validateParameters(array $data, array $required_keys) {
    foreach ($required_keys as $required_key) {
      if (!isset($data[$required_key])) {
        throw new \InvalidArgumentException('Missing parameter ' . $required_key . 'for charging a profile.');
      }
    }
  }

  /**
   * Gets the minor units converter.
   *
   * @return \Drupal\commerce_price\MinorUnitsConverterInterface
   *   The minor units converter.
   */
  protected function getMinorUnitsConverter() {
    return \Drupal::service('commerce_price.minor_units_converter');
  }

}
