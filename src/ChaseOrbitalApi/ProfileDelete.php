<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

/**
 * Defines the ProfileDelete request.
 */
class ProfileDelete extends RequestBase {

  /**
   * {@inheritdoc}
   */
  public function getParameters(array $data) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();

    $profile_delete_request = new \stdClass();
    $profile_delete_request->bin = $configuration['bin'];
    $profile_delete_request->version = '4.0';
    $profile_delete_request->merchantID = $configuration['merchant_id'];
    $profile_delete_request->orbitalConnectionUsername = $configuration['api_username'];
    $profile_delete_request->orbitalConnectionPassword = $configuration['api_password'];
    $profile_delete_request->customerRefNum = $data['remote_id'];
    $profile_delete = new \stdClass();
    $profile_delete->profileDeleteRequest = $profile_delete_request;
    return $profile_delete;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return ['remote_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestType() {
    return 'ProfileDelete';
  }

}
