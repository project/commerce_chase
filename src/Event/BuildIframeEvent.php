<?php

namespace Drupal\commerce_chase\Event;

use Drupal\commerce\EventBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Chase iframe event.
 *
 * @see \Drupal\commerce_chase\Event\PaymentEvents
 */
class BuildIframeEvent extends EventBase {

  /**
   * The form array.
   *
   * @var array
   */
  protected $form;

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * Constructs a new PaymentEvent.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function __construct(array $form, FormStateInterface $form_state) {
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Gets the form.
   *
   * @return array
   *   The form.
   */
  public function getForm() {
    return $this->form;
  }

  /**
   * Gets the form_state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state.
   */
  public function getFormState() {
    return $this->formState;
  }

}
