<?php

namespace Drupal\commerce_chase\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Defines the Chase iframe query event.
 *
 * @see \Drupal\commerce_chase\Event\PaymentEvents
 */
class BuildIframeQueryEvent extends EventBase {

  /**
   * The payment method.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentMethodInterface
   */
  protected $paymentMethod;

  /**
   * The url query array.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs a new PaymentEvent.
   *
   * @param array $query
   *   The query.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   */
  public function __construct(array $query, PaymentMethodInterface $payment_method) {
    $this->query = $query;
    $this->paymentMethod = $payment_method;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface
   *   The payment.
   */
  public function getPaymentMethod() {
    return $this->paymentMethod;
  }

  /**
   * Gets the url query array.
   *
   * @return array
   *   The url query array.
   */
  public function getQuery() {
    return $this->query;
  }

}
