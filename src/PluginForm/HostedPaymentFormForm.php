<?php

namespace Drupal\commerce_chase\PluginForm;

use Drupal\commerce_chase\Event\BuildIframeEvent;
use Drupal\commerce_chase\Event\BuildIframeQueryEvent;
use Drupal\commerce_chase\Event\ChaseEvents;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment method add form plugin.
 */
class HostedPaymentFormForm extends PaymentMethodAddForm implements ContainerInjectionInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $token;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The guzzle http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->token = $container->get('csrf_token');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->httpClient = $container->get('http_client');
    $instance->time = $container->get('datetime.time');
    $instance->moduleExtensionList = $container->get('extension.list.module');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element['card_type'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-card-type'],
      ],
    ];
    $element['card_number'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-card-number'],
      ],
    ];
    $element['expiration_month'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-exp-month'],
      ],
    ];
    $element['expiration_year'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-exp-year'],
      ],
    ];
    $element['remote_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-remote-id'],
      ],
    ];

    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $form_state->getBuildInfo()['callback_object'];
    $order = $checkout_flow->getOrder();
    $element['#order'] = $order;

    $remote_id = $order->id() . '-' . $this->time->getRequestTime();
    $library = $this->entity->getPaymentGateway()->getPlugin()->getMode() === 'live' ? 'commerce_chase/hosted-payment-form-live' : 'commerce_chase/hosted-payment-form-test';
    $element['iframe'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $this->buildCheckoutIframeUrl($element, $form_state, $remote_id),
        'name' => 'embedded-orbital-form',
        'scrolling' => 'no',
        'frameborder' => 0,
        'class' => ['commerce-chase-orbital-hpf-iframe'],
        'id' => 'commerce-chase-orbital-hpf-iframe',
        'width' => '490px',
        'height' => '270px',
      ],
      '#attached' => [
        'library' => [$library],
        'drupalSettings' => [
          'commerce_chase' => [
            'remoteId' => $remote_id,
          ],
        ],
      ],
    ];
    $event = new BuildIframeEvent($element, $form_state);
    $this->eventDispatcher->dispatch($event, ChaseEvents::BUILD_IFRAME);
    return $element;
  }

  /**
   * Builds the hosted form iFrame URL.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $remote_id
   *   The remote payment method id (aka customer reference number).
   *
   * @return string
   *   URL for hosted form iFrame.
   */
  protected function buildCheckoutIframeUrl(array $element, FormStateInterface $form_state, string $remote_id) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_method->getPaymentGateway()->getPlugin();
    $payment_gateway_configuration = $payment_method->getPaymentGateway()->getPluginConfiguration();

    $order = $element['#order'];
    // Discern the allowed card types.
    $allowed_types = array_filter($payment_gateway_configuration['allowed_types']);
    $host = $this->request->getSchemeAndHttpHost();
    $base_url = $payment_gateway_plugin->serviceUrl();
    $query = [
      'hostedSecureID' => $payment_gateway_configuration['hosted_secure_id'],
      'action' => 'buildForm',
      'css_url' => $host . '/' . $this->moduleExtensionList->getPath('commerce_chase') . '/css/hpf.css',
      'allowed_types' => implode('|', array_values($allowed_types)),
      'collectAddress' => 0,
      'required' => $payment_gateway_configuration['required'],
      'cardIndicators' => 'N',
      'hosted_tokenize' => 'store_only',
      'formType' => 5,
      'customer_email' => $order->getEmail(),
      'sessionId' => $this->token->get($order->id()),
      'customerRefNum' => $remote_id,
    ];

    // Allow modules to alter the query parameters for the iframe.
    $event = new BuildIframeQueryEvent($query, $payment_method);
    $this->eventDispatcher->dispatch($event, ChaseEvents::BUILD_IFRAME_QUERY);

    return Url::fromUri($base_url, ['query' => $query])->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

}
